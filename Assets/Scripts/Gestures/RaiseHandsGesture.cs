using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectProject
{
	public class RaiseHandsGesture : Gesture {
	

		
		public override event System.EventHandler<System.EventArgs> OnGesture;
		
		/// <summary>
		/// updateGesture internal automation
		/// </summary>
		private enum Step {Canceled, LowerThanHead, HigherThanHead, GestureTriggered}
		private Step step;
		private float clock; //time buffer
		
		/// <summary>
		/// Raise an event when the the gesture is completed
		/// </summary>
		protected void updateGestureInfo()
		{
			float rightHandY = this.GetComponent<AvatarControllerClassic>().HandRight.transform.position.y;
			float leftHandY = this.GetComponent<AvatarControllerClassic>().HandLeft.transform.position.y;
			float headY = this.GetComponent<AvatarControllerClassic>().Head.transform.position.y;
			switch (step)
			{
			case Step.LowerThanHead:
				//test if hands are close to eaach other
				if (rightHandY > headY && leftHandY > headY)
				{
					//wait for confirmation (timer)
					if (clock >= minTimeConfirm)
					{
						step = Step.HigherThanHead;
						clock = 0;
					}
				}
				
				clock += Time.deltaTime;
				break;
				
			case Step.HigherThanHead:
				if (rightHandY < headY && leftHandY < headY)
				{
					//wait for confirmation (timmer)
					if (clock > minTimeConfirm)
					{
						step = Step.GestureTriggered;
						clock = 0;
					}
				}
				
				clock += Time.deltaTime;
				break;
				
			case Step.GestureTriggered:
				OnGesture.Invoke(this, null);
				
				//Debug.Log("Left swipe triggered");
				
				step = Step.LowerThanHead;
				clock = 0;
				break;
				
			case Step.Canceled:	
				if (leftHandY < headY && rightHandY < headY)
				{
					step = Step.LowerThanHead;
					clock = 0;
				}
				break;
			}
			
			//Watchdog
			//TODO temps total
			if (clock > maxTimeConfirm)
			{
				step = Step.Canceled;
				clock = 0;
			}
		}
		
		// Use this for initialization
		void Start () {
			clock = 0;
			step = Step.Canceled;
		}
		
		// Update is called once per frame
		void Update () {
			updateGestureInfo();
		}
	}
}
