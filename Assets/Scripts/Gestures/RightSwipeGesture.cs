using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectProject
{
	public class RightSwipeGesture : Gesture {

		/// <summary>
		/// Needed distance between the right hand and the right elbow for the swipe to be triggered
		/// </summary>
		[Tooltip("Needed distance between the right hand and the right elbow for the swipe to be triggered")]
		public float SwipeDistance = 0.1f;

		

		public override event System.EventHandler<System.EventArgs> OnGesture;

		private float swipeDistance_
		{
			get
			{
				return SwipeDistance * getPlayerScale();
			}
		}
		
		/// <summary>
		/// updateGesture internal automation
		/// </summary>
		private enum Step {Canceled, CloseToBody, Stretched, GestureTriggered}
		private Step step;
		private float clock; //time buffer
	
		/// <summary>
		/// Raise an event when the the gesture is completed
		/// </summary>
		protected void updateGestureInfo()
		{
			float rightHandX = this.GetComponent<AvatarControllerClassic>().HandRight.transform.position.x;
			float rightElbowX = this.GetComponent<AvatarControllerClassic>().ElbowRight.transform.position.x;
			switch (step)
			{
			case Step.CloseToBody:
				//test if hands are close to eaach other
				if ((rightHandX - rightElbowX) > swipeDistance_)
				{
					//wait for confirmation (timer)
					if (clock >= minTimeConfirm)
					{
						step = Step.Stretched;
						clock = 0;
					}
				}
				
				clock += Time.deltaTime;
				break;
				
			case Step.Stretched:
				if (rightHandX < rightElbowX)
				{
					//wait for confirmation (timmer)
					if (clock > minTimeConfirm)
					{
						step = Step.GestureTriggered;
						clock = 0;
					}
				}
				
				clock += Time.deltaTime;
				break;
				
			case Step.GestureTriggered:
				OnGesture.Invoke(this, null);

				//Debug.Log("Right swipe triggered");
				
				step = Step.CloseToBody;
				clock = 0;
				break;
				
			case Step.Canceled:	
				if ((rightHandX - rightElbowX) < swipeDistance_)
				{
					step = Step.CloseToBody;
					clock = 0;
				}
				break;
			}
			
			//Watchdog
			//TODO temps total
			if (clock > maxTimeConfirm)
			{
				step = Step.Canceled;
				clock = 0;
			}
		}

		// Use this for initialization
		void Start () {
			clock = 0;
			step = Step.Canceled;
		}
		
		// Update is called once per frame
		void Update () {
			updateGestureInfo();
		}
	}
}
