using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectProject
{
	public class RunGesture : Gesture {
		
		/// <summary>
		/// Needed distance between the right hand and the left hand for the run to be triggered
		/// </summary>
		[Tooltip("Needed distance between the right hand and the left hand for the run to be triggered")]
		public float HandsRunDistance = 0.1f;
		

		
		public override event System.EventHandler<System.EventArgs> OnGesture;
		
		private float handsRunDistance_
		{
			get
			{
				return HandsRunDistance * getPlayerScale();
			}
		}
		
		/// <summary>
		/// updateGesture internal automation
		/// </summary>
		private enum Step {Canceled, RightHandForward, LeftHandForward, GestureTriggered}
		private Step step;
		private float clock; //time buffer
		
		/// <summary>
		/// Raise an event when the the gesture is completed
		/// </summary>
		protected void updateGestureInfo()
		{
			float rightHandY = this.GetComponent<AvatarControllerClassic>().HandRight.transform.position.y;
			float leftHandY = this.GetComponent<AvatarControllerClassic>().HandLeft.transform.position.y;
			switch (step)
			{
			case Step.RightHandForward:
				//test if hands are close to eaach other
				if ((leftHandY - rightHandY) > handsRunDistance_)
				{
					//wait for confirmation (timer)
					if (clock >= minTimeConfirm)
					{
						step = Step.LeftHandForward;
						clock = 0;
					}
				}
				
				clock += Time.deltaTime;
				break;
				
			case Step.LeftHandForward:
				if ((rightHandY - leftHandY) > handsRunDistance_)
				{
					//wait for confirmation (timmer)
					if (clock > minTimeConfirm)
					{
						step = Step.GestureTriggered;
						clock = 0;
					}
				}
				
				clock += Time.deltaTime;
				break;
				
			case Step.GestureTriggered:
				OnGesture.Invoke(this, null);
				
				//Debug.Log("Run triggered");
				
				step = Step.RightHandForward;
				clock = 0;
				break;
				
			case Step.Canceled:	
				if ((rightHandY - leftHandY) > handsRunDistance_)
				{
					step = Step.RightHandForward;
					clock = 0;
				}
				break;
			}
			
			//Watchdog
			//TODO temps total
			if (clock > maxTimeConfirm)
			{
				step = Step.Canceled;
				clock = 0;
			}
		}
		
		// Use this for initialization
		void Start () {
			clock = 0;
			step = Step.Canceled;
		}
		
		// Update is called once per frame
		void Update () {
			updateGestureInfo();
		}
	}
}
