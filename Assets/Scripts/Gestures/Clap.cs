using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace KinectProject
{
    public class Clap : Gesture
    {
        /// <summary>
        /// Distance between hands within the clap is triggered
        /// </summary>
        [Tooltip("Distance between hands within the clap is triggered")]
        public float ClappingDistance = 0.1f;

        /// <summary>
        /// Distance between hands beyond which they are considered separated
        /// </summary>
        [Tooltip("Distance between hands beyond which they are considered separated")]
        public float SeparateDistance = 1;


        public override event EventHandler<EventArgs> OnGesture;

        
        private float clappingDistance_
        {
            get
            {
                return ClappingDistance * getPlayerScale();
            }
        }

        private float separateDistance_
        {
            get
            {
                return SeparateDistance * getPlayerScale();
            }
        }

        /// <summary>
        /// updateGesture internal automation
        /// </summary>
        private enum Step {Canceled, DetatchedHands, JoinedHand, GestureTriggered}
        private Step step = Step.Canceled;
        private float clock = 0; //time buffer


        /// <summary>
        /// Raise an event when the the gesture is completed
        /// </summary>
        protected void updateGestureInfo()
        {
            Vector3 leftHand = this.GetComponent<AvatarControllerClassic>().HandLeft.position;
            Vector3 rightHand = this.GetComponent<AvatarControllerClassic>().HandRight.position;
            switch (step)
            {
                case Step.DetatchedHands:
                    //test if hands are close to eaach other
                    if (Vector3.Distance(leftHand, rightHand) <= clappingDistance_)
                    {
                        //wait for confirmation (timer)
                        if (clock >= minTimeConfirm)
                        {
                            step = Step.JoinedHand;
                            clock = 0; 
                        }
                    }

                    clock += Time.deltaTime;
                    break;

                case Step.JoinedHand:
                    if (Vector3.Distance(leftHand, rightHand) > separateDistance_)
                    {
                        //wait for confirmation (timmer)
                        if (clock > minTimeConfirm)
                        {
                            step = Step.GestureTriggered;
                            clock = 0;
                        }
                    }

                    clock += Time.deltaTime;
                    break;

                case Step.GestureTriggered:

                    
                    this.OnGesture.Invoke(this, null);

                    step = Step.Canceled;
                    clock = 0;
                    break;

                case Step.Canceled:
                    if (Vector3.Distance(leftHand, rightHand) > separateDistance_)
                    {
                        step = Step.DetatchedHands;
                        clock = 0;
                    }
                    break;
            }

            //Watchdog
            if (clock > maxTimeConfirm)
            {
                step = Step.Canceled;
                clock = 0;
            }
        }



        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            updateGestureInfo();
        }
        
    }





}
