using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectProject
{
    public class RightHit : Gesture
    {
        public override event EventHandler<EventArgs> OnGesture;
        public float armingDistance = 0.5f;
        public float hitDistance = 1.5f;

        enum State { off, armed, hit}
        State state = State.off;


        Vector3 rightHand;
        Vector3 rightShoulder;

        /// <summary>
        /// Time gestion
        /// </summary>
        float clock = 0f;

        private void Update()
        {
            //Automation
            switch (state)
            {
                case State.off:

                    rightHand = this.GetComponent<AvatarControllerClassic>().HandRight.position;
                    rightShoulder = this.GetComponent<AvatarControllerClassic>().ShoulderRight.position;

                    if (Vector3.Distance(rightHand, rightShoulder) < armingDistance * getPlayerScale())
                    {
                        if (clock >= minTimeConfirm)
                        {
                            clock = 0;
                            state = State.armed;
                        }
                    }

                    break;


                case State.armed:

                    rightHand = this.GetComponent<AvatarControllerClassic>().HandRight.position;
                    rightShoulder = this.GetComponent<AvatarControllerClassic>().ShoulderRight.position;

                    float dist = Vector3.Distance(rightHand, rightShoulder);
                    if (dist > hitDistance * getPlayerScale())
                    {
                        Vector3 forward = this.transform.forward;// this.GetComponent<AvatarControllerClassic>().ShoulderCenter.forward;
                       // Debug.Log(forward);
                        Debug.Log(Vector3.Dot((rightHand - rightShoulder), forward) + "      " + 0.8f * dist);
                        if (Vector3.Dot((rightHand - rightShoulder), forward) >= 0.8f * dist)
                        {
                            if (clock >= minTimeConfirm)
                            {
                                clock = 0;
                                state = State.hit;
                            }
                        }
                    }

                    break;


                case State.hit:
                    
                    OnGesture.Invoke(this, null);
                    state = State.off;

                    break;
            }


            //Watchdog
            if (clock > maxTimeConfirm)
            {
                clock = 0;
                state = State.off;
            }

            clock += Time.deltaTime;
        }



    }
}