using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectProject
{

    public class BodyInfo
    {

        /// <summary>
        /// Return true if the orthogonal projection of p on the [min max] line is within the segment [min max]
        /// </summary>
        /// <param name="p"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        static private bool isBetween(Vector3 p, Vector3 min, Vector3 max)
        {
            float pproj = Vector3.Dot(p - min, max - min);

            return (pproj >= 0) && (pproj <= (max - min).magnitude);
        }

        /// <summary>
        /// Return true if a point is between the two shoulders of a classic avatar
        /// </summary>
        /// <param name="leftShoulder"></param>
        /// <param name="rightShoulder"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        static public bool isBetweenShoulders(Vector3 point, Vector3 leftShoulder, Vector3 rightShoulder)
        {
            return isBetween(point, leftShoulder, rightShoulder);
        }


    }
}