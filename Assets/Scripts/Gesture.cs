using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectProject
{




    public abstract class Gesture : MonoBehaviour
    {
        /// <summary>
        /// Minimum amount of time to aknowledge the gesture (in secs)
        /// </summary>
        public float minTimeConfirm = 0.1f;

        /// <summary>
        /// Maximum amount of time to realise the gesture (in secs)
        /// </summary>
        public float maxTimeConfirm = 1f;


        protected float getPlayerScale()
        {
            float playerScale = Vector3.Distance(
                this.GetComponent<AvatarControllerClassic>().ShoulderLeft.transform.position,
                this.GetComponent<AvatarControllerClassic>().ShoulderRight.transform.position);

            return playerScale;
        }


        /// <summary>
        /// Event raised when the gesture is executed
        /// </summary>
        abstract public event System.EventHandler<System.EventArgs> OnGesture;
        
        
    }

}