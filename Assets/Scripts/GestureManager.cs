using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace KinectProject
{
    
    public class Displayable 
    {
        /// <summary>
        /// Gesture to display
        /// </summary>
        public string gesture;



        /// <summary>
        /// True if the gesture has been triggered
        /// </summary>
        public bool state = false;

        public Displayable (string g)
        {
            gesture = g;
        }


        float clock = 0;
        float displayTime = 1;
        bool oldState = false;

        private void Update()
        {
            if (state)
            {
                //rising front
                if (!oldState)
                {
                    clock = 0;
                }


                if (clock >= displayTime)
                {
                    clock = 0;
                    state = false;
                }

                clock += Time.deltaTime;
            }


            oldState = state;
        }


        public void Display(int index)
        {
            string s = state ? " : OK" : " : NO";
            Color c = state ? Color.green : Color.red;
            {
                TrackingDisplayer.DisplayText(gesture+s, c, 10, 20 * (index + 2));
            }

            Update();
        }


    }
    
    
    public class GestureManager : MonoBehaviour
    {


        //gestures
        private Clap clap;
        private LeftSwipeGesture lefSwipe;
        private RightSwipeGesture rightSwipe;
        private RunGesture run;
        private RaiseHandsGesture raiseHand;
        private RightHit rightHit;

        private Displayable clapDisplay;
        private Displayable lefSwipeDisplay;
        private Displayable rightSwipeDisplay;
        private Displayable runDisplay;
        private Displayable raiseHandDisplay;
        private Displayable rightHitDisplay;


        // Use this for initialization
        void Start()
        {
            //Getting Gestures 
            clap = this.GetComponent<Clap>();
            lefSwipe = this.GetComponent<LeftSwipeGesture>();
            rightSwipe = this.GetComponent<RightSwipeGesture>();
            run = this.GetComponent<RunGesture>();
            raiseHand = this.GetComponent<RaiseHandsGesture>();
            rightHit = this.GetComponent<RightHit>();

            clapDisplay         = new Displayable("Clap");
            lefSwipeDisplay     = new Displayable("LefSwipe"); 
            rightSwipeDisplay   = new Displayable("RightSwipe"); 
            runDisplay          = new Displayable("Run"); 
            raiseHandDisplay    = new Displayable("RaiseHand");
            rightHitDisplay     = new Displayable("Right Hit");


            //Event gestion
            clap.OnGesture += OnClap;
            lefSwipe.OnGesture += OnSwipeLeft;
            rightSwipe.OnGesture += OnSwipeRight;
            run.OnGesture += OnRun;
            raiseHand.OnGesture += OnRaiseHands;
            rightHit.OnGesture += OnRightHit;

        }

        // Update is called once per frame
        void Update()
        {

        }


        /// <summary>
        /// Called when the user clap
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnClap(object sender, System.EventArgs args)
        {
            clapDisplay.state = true;

            Menu.BackToMenu();
        }

        /// <summary>
        /// Called when the user swipe left
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnSwipeLeft(object sender, System.EventArgs args)
        {
            lefSwipeDisplay.state = true;
        }

        /// <summary>
        /// Called when the user swipe right
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnSwipeRight(object sender, System.EventArgs args)
        {
            rightSwipeDisplay.state = true;
        }

        /// <summary>
        /// Called when the user run
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnRun(object sender, System.EventArgs args)
        {
            runDisplay.state = true;
        }

        /// <summary>
        /// Called when the user raise both hands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnRaiseHands(object sender, System.EventArgs args)
        {
            raiseHandDisplay.state = true;
        }

        /// <summary>
        /// Called when the user raise both hands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void OnRightHit(object sender, System.EventArgs args)
        {
            rightHitDisplay.state = true;
        }


        private void OnGUI()
        {
            clapDisplay.Display(0);
            lefSwipeDisplay.Display(1);
            rightSwipeDisplay.Display(2);
            runDisplay.Display(3);
            raiseHandDisplay.Display(4);
            rightHitDisplay.Display(5);

        }




    }
}