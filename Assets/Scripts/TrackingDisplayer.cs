using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KinectProject
{
    public class TrackingDisplayer : MonoBehaviour
    {


        /// <summary>
        /// True if the player is tracked by the kinect
        /// </summary>
        private bool playerTracked = false;

        private static TrackingDisplayer instance;



        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        // Use this for initialization
        void Start()
        {
        }

        // Update is called once per frame
        void Update()
        {
            playerTracked = KinectManager.Instance.IsUserDetected();
        }


        /// <summary>
        /// Display text in a box at (posX, posY)
        /// </summary>
        /// <param name="s">string to display</param>
        /// <param name="posX">x coordinate</param>
        /// <param name="posY">y coordinate</param>
        private void DisplayText(string s, float posX, float posY)
        {
            GUIContent trackingMsg = new GUIContent(s);

            GUIStyle style = GUI.skin.box;
            style.alignment = TextAnchor.MiddleCenter;
            
            Vector2 size = style.CalcSize(trackingMsg);

            GUI.Box(new Rect(posX, posY, size.x, size.y), s, style);

        }

        /// <summary>
        /// Display text in a box at (posX, posY)
        /// </summary>
        /// <param name="s">string to display</param>
        /// <param name="posX">x coordinate</param>
        /// <param name="posY">y coordinate</param>
        /// <param name="c">Font color</param>
        public static void DisplayText(string s, Color c, float posX, float posY)
        {
            GUIContent trackingMsg = new GUIContent(s);

            GUIStyle style = GUI.skin.box;
            style.alignment = TextAnchor.MiddleCenter;
            style.normal.textColor = c;

            Vector2 size = style.CalcSize(trackingMsg);

            GUI.Box(new Rect(posX, posY, size.x, size.y), s, style);
            
        }







        private void OnGUI()
        {
            DisplayText("Player Tracking : " + (playerTracked ? "OK" : "NO"), (playerTracked ? Color.green : Color.red), 10, 10);

        }
    }
}