using UnityEngine;
using System.Collections;

public class HighHandDetection : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (gameObject.GetComponent<AvatarControllerClassic> ().HandRight.transform.position.y > gameObject.GetComponent<AvatarControllerClassic> ().ShoulderRight.transform.position.y)
			Debug.Log ("Right hand higher than the right shoulder");
	}
}
