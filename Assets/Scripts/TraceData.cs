using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceData : MonoBehaviour {

    [Tooltip("File to write in")]
    public string path;

    [Tooltip("Tracing period (each frame if <= 0)")]
    public float period;
    


    //-------------------------------

    private System.IO.StreamWriter file;
    private float clock = 0;


    // Use this for initialization
    void Start () {
		file = new System.IO.StreamWriter(@path);
        Debug.Log(@path);
        file.WriteLine("Tracing " + this.gameObject.name + " (" + System.DateTime.Now + ")");
        file.WriteLine("Time         "+
            "   " + "X        " +
            "   " + "Y        " +
            "   " + "Z        ");
    }
	
	// Update is called once per frame
	void Update () {

        if (clock >= period)
        {
            WriteInfos();
            clock = 0;
        }


        clock += Time.deltaTime;
	}

    void WriteInfos()
    {
        file.WriteLine(Time.realtimeSinceStartup.ToString("0.000000") 
            + "   " + this.transform.position.x.ToString("0.000000")
            + "   " + this.transform.position.y.ToString("0.000000")
            + "   " + this.transform.position.z.ToString("0.000000"));
    }

    private void OnDestroy()
    {
        file.Close();
    }
}
